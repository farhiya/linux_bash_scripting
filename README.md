# NGINX bash_scripting

In this README there is a step by step guide to create and run a bash script in ubuntu. You will download nginx and replace the generic web page with your webpage.

1. Inside the bash terminal where you want to create the folder:

   - Create a new folder for nginx scripting
      - ` mkdir ubuntu_bash_scripting`

   - Go into nginx folder and create a README.md, a bash script for the apache download and a folder with your website.

    - `touch README.md nginx_bash_script.sh`
    - `mkdir website`

    - Inside the website folder create an index.html file that you want replace the generic nginx welcome page with.

      - `cd website`
      - `touch index.html`

    - Connect to your repo

2. Update files in VSCode
   
    - Configure you're bash script with the following:
    
        ```bash
            # update the repositories 
            sudo apt-get update
            sudo apt upgrade -y

            # install ngixn
            sudo apt install nginx -y

            # start nginx
            sudo systemctl start nginx

            # Move the file into the correct location
            sudo mv ~/website/index.html /var/www/html/index.html 

            # restart nginx
            sudo systemctl restart nginx

        ```
    - Configure your html file
    - Save all files

3. Connect to ubuntu in a new terminal
   
    Make sure you use your own IP address and your own pem
   
    - `ssh -i ~/.ssh/ch9_shared.pem ubuntu@54.216.220.146` 
    
    You are now in a ubuntu terminal. At this point we should have 2 seperate terminal windows. One for bash and one for ubuntu. This will make the next few stages easier.

4. Moving files and folders from local to ubuntu machine
   
   This should be done in our BASH terminal

        ```bash

        # saves the bash script in ubuntu
        `scp -i ~/.ssh/ch9_shared.pem nginx_bash_script.sh ubuntu@54.216.220.146:/home/ubuntu/`

        # saves the website folder in ubuntu
        `scp -i ~/.ssh/ch9_shared.pem -r website ubuntu@54.216.220.146:/home/ubuntu`

        ```
        
5. In ubuntu terminal, give the bash script correct permissions
   
    - Give permissions for to the bash file 
        `chmod +x nginx_bash_script.sh`

6. In the BASH terminal run the bash script in the background in ubuntu 

    - This will run bash and your new html will replace the generic nginx web page

        `ssh -i ~/.ssh/ch9_shared.pem ubuntu@54.216.220.146 ./nginx_bash_script.sh`

7. Go to the ip address and your new page should appear.
    - In the web browser : 54.216.220.146

8. Save everything in your repo. Use the BASH terminal.

cd test 1
